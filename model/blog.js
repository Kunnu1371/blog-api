const mongoose = require("mongoose");
const { ObjectId } = mongoose.Schema;

const blogSchema = new mongoose.Schema(
  {
    title: {
      type: String,
      trim: true,
      required: true,
    },

    createdBy: {
      type: ObjectId,
      ref: "Admin",
      required: true,
    },

    paragraph: {
      type: String,
      trim: true,
      required: true,
    },

    photo: {
      type: String,
      required: true,
    },
    
    contentType: {
      type: String,
      required: true
    }

  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("Blog", blogSchema);