const router = require("express").Router();

const {
  signup,
  signin,
  read,
  remove,
  update,
  adminById,
} = require("../controllers/admin");
const { requireSignin, isAdmin, isAuth } = require("../controllers/authAdmin");
const { signupValidator } = require("../validator/index");

router.post("/signup", signupValidator, signup);
router.post("/signin", signin);
router.get("/read/:adminId", requireSignin, isAdmin, isAuth, read);
router.put("/update/:adminId", requireSignin, isAdmin, isAuth, update);
router.delete("/remove/:adminId", requireSignin, isAdmin, isAuth, remove);

router.param("adminId", adminById);

module.exports = router;
