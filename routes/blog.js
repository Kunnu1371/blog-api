const router = require("express").Router();
const {
  update,
  read,
  remove,
  getAllBlogs,
  blogById,
} = require("../controllers/blog");
const Admin = require("../model/admin");
const Blog = require("../model/blog");
const { adminById } = require("../controllers/admin");
const { requireSignin, isAdmin, isAuth } = require("../controllers/authAdmin");

const multer = require("multer");
const path = require("path");
// Set The Storage Engine
const storage = multer.diskStorage({
  destination: "./uploads/",
  filename: function (req, file, cb) {
    cb(
      null,
      file.fieldname + "-" + Date.now() + path.extname(file.originalname)
    );
  },
});

// Init Upload
const upload = multer({
  storage: storage,
  limits: { fileSize: 1024 * 1024 },
  fileFilter: function (req, file, cb) {
    checkFileType(file, cb);
  },
});

// Check File Type
function checkFileType(file, cb) {
  const filetypes = /jpeg|jpg|png/;
  const extname = filetypes.test(path.extname(file.originalname).toLowerCase());
  const mimetype = filetypes.test(file.mimetype);
  if (mimetype && extname) {
    return cb(null, true);
  } else {
    cb("Only images with PNG, JPG, and JPEG extentions are allowed!");
  }
}

router.post(
  "/create/:adminId",
  requireSignin,
  isAdmin,
  isAuth,
  upload.single("photo"),
  async function (req, res) {
    const file = req.file;
    if (req.body.title == "" || req.body.paragraph == "" || file == undefined) {
      return res.status(404).json({
        error: "All fields are required",
      });
    }
    const blog = {
      title: req.body.title,
      createdBy: req.profile._id,
      paragraph: req.body.paragraph,
      photo: __dirname + "\\" + file.path,
      contentType: file.mimetype,
    };
    console.log(file);
    const createdBlog = new Blog(blog);
    await createdBlog.save((err, created) => {
      if (err) return res.status(500).json(err);
      return res.status(201).json({
        status: "success",
        message: "Blog has been created successfully.",
        created
      });
    });
  }
);

router.put(
  "/update/:blogId/:adminId",
  requireSignin,
  isAdmin,
  isAuth,
  upload.single("photo"),
  async function (req, res) {
    const file = req.file;
    if (req.body.title == "" || req.body.paragraph == "" || file == undefined) {
      return res.status(404).json({
        error: "All fields are required",
      });
    }
   
    Blog.findOneAndUpdate(
      { _id: req.blog._id },
      { $set: req.body, photo: __dirname + "\\" + file.path, contentType: file.mimetype},
      { new: true }
    ).exec((err, updated) => {
      if (err) return res.status(500).json(err);
      return res.status(201).json({
        status: "success",
        message: "Blog has been updated successfully.",
        updated,
      });
    });
  }
);

router.get("/read/:blogId", read);
router.get("/blogs", getAllBlogs);
router.delete(
  "/delete/:blogId/:adminId",
  requireSignin,
  isAdmin,
  isAuth,
  remove
);

router.param("blogId", blogById);
router.param("adminId", adminById);

module.exports = router;
