const Admin = require("../model/admin");
const Blog = require("../model/blog");

exports.blogById = (req, res, next, id) => {
  Blog.findById(id).exec((err, blog) => {
    if (err || !blog) {
      return res.status(404).json({
        error: "Blog not found",
      });
    }
    req.blog = blog;
    next();
  });
};

exports.read = async (req, res) => {
  const blogId = req.params.blogId;
  const blog = await Blog.findById(blogId).populate("createdBy");
  if (blog) {
    return res.status(200).json({
      status: "success",
      blog,
    });
  } else {
    return res.status(404).json({
      error: "not found",
    });
  }
};

exports.remove = async(req, res) => {
    const blogId = req.params.blogId;
    const blog = await Blog.findById(blogId)
    blog.remove((err, deleted) => {
        if(err) return res.status(500).json(err)
        return res.status(200).json({
            status: "success",
            message: "The blog has been deleted successfully."
        })
    })
};

exports.getAllBlogs = async(req, res) => {
    const blogs = await Blog.find().populate('createdBy')
    if(blogs) {
        return res.status(200).json({
            status: "success",
            total: blogs.length,
            blogs
        })
    }
};
