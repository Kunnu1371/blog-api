const Admin = require("../model/admin");
const jwt = require("jsonwebtoken");

exports.adminById = (req, res, next, id) => {
  Admin.findById(id).exec((err, admin) => {
    if (err || !admin) {
      return res.status(404).json({
        error: "Admin not found",
      });
    }
    req.profile = admin;
    next();
  });
};

exports.signup = async (req, res) => {
  const adminFound = await Admin.findOne(
    { email: { $regex: `^${req.body.email}$`, $options: "i" } },
    (err, admin) => {
      if (err) return res.status(500).json(err);
    }
  );
  if (adminFound) {
    // console.log("userFound", adminFound)
    return res
      .status(400)
      .json("Admin with that email exist in database. Please login");
  } else {
    const admin = new Admin(req.body);
    admin.save((err, admin) => {
      if (err) {
        return res.status(500).json({
          err: errorHandler(err),
        });
      }
      admin.salt = undefined;
      admin.hashed_Password = undefined;
      res.status(201).json({
        status: "success",
        message: "Admin has been registered",
        admin,
      });
      console.log(admin);
    });
  }
};

exports.signin = async (req, res) => {
  // Check user based on email
  const { email, password } = req.body;
  Admin.findOne({ email }, (err, admin) => {
    if (err || !admin) {
      return res.status(404).json({
        error: "Admin with that email doesn't exist. Please signup",
      });
    }
    // If admin found
    if (!admin.authenticate(password)) {
      return res.status(401).json({
        error: "Email and password don't match",
      });
    }
    const token = jwt.sign({ _id: admin._id }, process.env.JWT_SECRET);
    res.cookie("t", token, { expire: new Date() + 9999 });
    const { _id, name, email, role } = admin;
    return res
      .status(200)
      .json({ status: "success", token, admin: { _id, email, name, role } });
  });
};

exports.update = async (req, res) => {
  await Admin.findOneAndUpdate(
    { _id: req.profile.id },
    { $set: req.body },
    { new: true },
    (err, admin) => {
      if (err) {
        return res.status(400).json({
          error: "You are not authorized to perform this action",
        });
      }
      admin.salt = undefined;
      admin.hashed_Password = undefined;
      res
        .status(200)
        .json({
          status: "success",
          message: "profile updated successfully",
          admin,
        });
    }
  );
};

exports.read = (req, res) => {
  const profile = req.profile;
  profile.hashed_Password = undefined;
  profile.salt = undefined;
  return res.status(200).json({ status: "success", profile });
};

exports.remove = (req, res) => {
  const admin = req.profile;
  try {
    admin.remove((err, removedAdmin) => {
      if (err) return res.status(500).json(err);
      return res.status(200).json({
        status: "success",
        message: "Admin profile has been deleted",
      });
    });
  } catch (err) {
    return res.status(404).json({ message: "Admin not found" });
  }
};
