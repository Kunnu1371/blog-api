const mongoose = require("mongoose");
require("dotenv").config();

const connection = async () => {
  const uri = process.env.MONGO_URI;
  try {
    const conn = await mongoose.connect(uri, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    });
    console.log("DB Connected!");
  } catch (e) {
    console.log(`Error occurred: ${e.message}`);
  }
};
connection();
