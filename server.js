const express = require("express");
const cookieParser = require("cookie-parser");
const expressValidator = require("express-validator");
const app = express();

app.use(express.json());
app.use(cookieParser());
app.use(expressValidator());

const adminRoute = require("./routes/admin");
const blogRoute = require("./routes/blog");

app.use("/api/admin", adminRoute);
app.use("/api/blog", blogRoute);

const port = process.env.PORT || 3000;
app.listen(port, () => {
  console.log(`Server started on port ${port}`);
  require("./db-connect.js");
});
